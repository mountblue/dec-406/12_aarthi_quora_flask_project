from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from elasticsearch import Elasticsearch
import os
from flask_babel import Babel
from flask_bcrypt import Bcrypt
from celery import Celery


app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost'
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
app.config['ELASTICSEARCH_URL']='http://localhost:9200'
ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
babel = Babel(app)
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])


bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'
app.config['POSTS_PER_PAGE'] = 10


celery = Celery(app.name, backend='redis://localhost', broker=app.config['CELERY_BROKER_URL'])


from quora import routes, models
