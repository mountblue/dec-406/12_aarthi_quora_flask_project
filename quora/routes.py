from flask import render_template, url_for, redirect, flash, request, current_app, jsonify, send_file
from quora.forms import LoginForm, QuestionForm, AnswerForm
from quora import login, login_manager, Celery, db, app
from quora.models import User, Question, Answer
from flask_login import current_user, login_user, logout_user, login_required
from quora.forms import RegistrationForm
from sqlalchemy import desc
from flask import g
from quora.forms import SearchForm
from flask_babel import get_locale, _
import os
from quora.tasks import download_data_task


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    g.search_form = SearchForm()
    g.locale = str(get_locale())


@app.route('/')
@app.route('/index')
def index():
    questions = Question.query.order_by(desc(Question.timestamp)).all()
    return render_template('index.html', questions=questions)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        user = User.query.filter_by(username=username, password=password).first()
        if user is None:
            flash('Incorrect details!')
            return redirect(url_for('index'))
        login_user(user, remember=form.remember_me.data)
        flash('Login successful!')
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    flash('Logout successful!')
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        flash('Account signed in')
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        email = form.email.data
        user = User(username=username, email=email, password=password)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/deletequestion/<questionid>', methods=['GET', 'POST'])
def delete_question(questionid):
    if current_user.is_authenticated:
        Question.query.filter_by(id=questionid).delete()
        db.session.commit()
        flash('Deleted Question!')
        return redirect(url_for('index'))
    return render_template('login.html', title='Login')


@app.route('/questions/<questionid>/edit', methods=['GET', 'POST'])
def edit_question(questionid):
    ques = Question.query.get_or_404(questionid)
    if current_user.is_authenticated:
        form = QuestionForm()
        if form.validate_on_submit():
            ques.body = form.question.data
            db.session.commit()
            flash('Edited Question!')
            return redirect(url_for('index'))
        return render_template('postques.html', title='Edit', form=form)
    return render_template('login.html', title='Login')


@app.route('/questions/post', methods=['GET', 'POST'])
def post_question():
    if current_user.is_authenticated:
        form = QuestionForm()
        if form.validate_on_submit():
            u = User.query.get(current_user.id)
            question = Question(body=form.question.data, user_id=current_user.id, username=u.username)
            db.session.add(question)
            db.session.commit()
            flash('Posted Question!')
            return redirect(url_for('index'))
        return render_template('postques.html', title='Post', form=form)
    return render_template('login.html', title='Login')


@app.route('/questions/<questionid>', methods=['GET', 'POST'])
def view_question(questionid):
    questions = Question.query.get(int(questionid))
    ans = Answer.query.filter_by(question_id=questionid).order_by(desc(Answer.timestamp))
    form = AnswerForm()
    if form.validate_on_submit():
        u = User.query.get(current_user.id)
        q = Question.query.get(int(questionid))
        answer = Answer(body=form.answer.data, user_id=current_user.id, username=u.username, question_id=q.id)
        db.session.add(answer)
        db.session.commit()
        flash('Posted Answer!')
        return render_template('viewcomment.html', questions=questions, answers=ans, title='Post', form=form)
    return render_template('viewcomment.html', questions=questions, answers=ans, title='Post', form=form)


@app.route('/deleteanswer/<answerid>', methods=['GET', 'POST'])
def delete_answer(answerid):
    ans = Answer.query.get_or_404(int(answerid))
    qid = ans.question_id
    if current_user.is_authenticated:
        Answer.query.filter_by(id=answerid).delete()
        db.session.commit()
        flash('Deleted Answer!')
        return redirect(url_for('view_question', questionid=qid))
    return render_template('login.html', title='Login')


@app.route('/questions/<questionid>/answers/<answerid>/edit', methods=['GET', 'POST'])
def edit_answer(answerid, questionid):
    ans = Answer.query.get_or_404(answerid)
    qid = ans.question_id
    if current_user.is_authenticated:
        form = AnswerForm()
        if form.validate_on_submit():
            ans.body = form.answer.data
            db.session.commit()
            flash('Edited Answer!')
            return redirect(url_for('view_question', questionid=qid))
        return render_template('postans.html', title='Edit', form=form)
    return render_template('login.html', title='Login')


@app.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('index'))
    page = request.args.get('page', 1, type=int)
    questions, total = Question.search(g.search_form.q.data, page, current_app.config['POSTS_PER_PAGE'])
    return render_template('search.html', title='Search', questions=questions)


@app.route("/download", methods=['POST'])
def download():
    download = download_data_task.delay(current_user.id)
    return jsonify({}), 202, {'Location': url_for('download_status', download_id=download.id)}


@app.route('/download/<path:filepath>', methods=['GET'])
def download_data(filepath):
    return send_file(filepath)


@app.route('/downlad_status/<download_id>')
def download_status(download_id):
    download = download_data_task.AsyncResult(download_id)
    # print("ready=",download.ready())
    status = download.ready()
    if not status:
        return jsonify({
            'status': 'PENDING'
        })
    else:
        result = download.get()
        filepath = result['filepath']
        # redirect to '/download/<filepath>`
    if download.state == 'PENDING':
        response = {
            'state': download.state,
            'status': 'Pending...'
        }
    elif download.state != 'FAILURE':
        response = {
            'state': download.state,
            'status': download.info.get('status', '')
        }
        if 'filepath' in download.info:
            response['filepath'] = download.info['filepath']
    else:
        response= {
            'state' : download.state,
            'status' : str(download.info)
        }
    return jsonify(response)
